﻿using UnityEngine;
using System.Collections;
using App.Scripts.Services;
using App.Scripts.Entities;

public class SceneController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //SQLiteService.DatabaseName = "Database.s3db";
        SQLiteService.DatabaseName = "existing.db";

        SQLiteService.CreateTable<Enemy>();

        EnemyRepository.Create(new Enemy() {
            Id = 1,
            Name = "Bad Guy #1",
            Type = "Bird",
        });

        EnemyRepository.Create(new Enemy[] {
            new Enemy() {
                Id = 2,
                Name = "Bad Guy #2",
                Type = "Dog",
            },
            new Enemy() {
                Id = 3,
                Name = "Bad Guy #3",
                Type = "Bird",
            },
            new Enemy() {
                Id = 4,
                Name = "Bad Guy #4",
                Type = "Humanoid",
            },
        });

        Debug.Log("Getting All");

        foreach (Enemy enemy in EnemyRepository.GetAll()) {
            Debug.Log(enemy.Name);
        }

        Debug.Log("Getting by Id");
        Enemy enemy1 = EnemyRepository.GetById(2);
        Debug.Log(enemy1.Name);


        Debug.Log("Getting by Name");
        Enemy enemy2 = EnemyRepository.GetByName("Bad Guy #3");
        Debug.Log(enemy2.Name);

        Debug.Log("Getting by Type");
        foreach (Enemy enemy in EnemyRepository.GetByType("Bird")) {
            Debug.Log(enemy.Name);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
