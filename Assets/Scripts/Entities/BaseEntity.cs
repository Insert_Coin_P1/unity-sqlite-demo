﻿using SQLite4Unity3d;

namespace App.Scripts.Entities
{
    public abstract class BaseEntity
    {
        [PrimaryKey]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
