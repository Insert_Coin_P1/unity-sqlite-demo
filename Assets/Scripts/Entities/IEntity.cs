﻿namespace App.Scripts.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
        string Name { get; set; }
    }
}
