﻿using App.Scripts.Entities;
using System.Collections.Generic;

namespace App.Scripts.Services
{
    public class EnemyRepository
    {
        public static Enemy Create(Enemy enemy)
        {
            SQLiteService.Connection.Insert(enemy);
            return enemy;
        }

        public static int Create(IEnumerable<Enemy> enemy)
        {
            return SQLiteService.Connection.InsertAll(enemy);
        }

        public static IEnumerable<Enemy> GetAll()
        {
            return SQLiteService.Connection.Table<Enemy>();
        }

        public static Enemy GetById(int id)
        {
            return SQLiteService.Connection.Table<Enemy>()
                .Where(x => x.Id == id)
                .FirstOrDefault();
        }

        public static Enemy GetByName(string name)
        {
            return SQLiteService.Connection.Table<Enemy>()
                .Where(x => x.Name == name)
                .FirstOrDefault();
        }

        public static IEnumerable<Enemy> GetByType(string type)
        {
            return SQLiteService.Connection.Table<Enemy>()
                .Where(x => x.Type == type);
        }
    }
}
